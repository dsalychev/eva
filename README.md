[![Coverity Scan Build Status](https://scan.coverity.com/projects/9742/badge.svg)](https://scan.coverity.com/projects/eva)

## What is Eva? ##

Eva is a voice-based game controller. It is based on idea that
player has a mouth as an input device. You can use configuration
file to control what combinations of keys will be "pressed" as
a reaction on some phrases recognized.
