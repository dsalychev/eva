/*
 * Eva
 *
 * Copyright (C) 2016, Dmitry Salychev <darkness.bsd@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <pocketsphinx.h>

#include "eva.h"

#define LM_MAGICKA2		"./game/magicka2/magicka2.lm"
#define DIC_MAGICKA2		"./game/magicka2/magicka2.dic"

int main(int argc, char *argv[])
{
	ps_decoder_t *ps = NULL;
	cmd_ln_t *config = NULL;

	config = cmd_ln_init(NULL, ps_args(), TRUE,
		 "-hmm", "/usr/local/share/pocketsphinx/model/en-us/en-us",
		 "-lm", LM_MAGICKA2,
		 "-dict", DIC_MAGICKA2,
		 NULL);
	if (config == NULL) {
		fprintf(stderr, "Failed to create recognizer, see log \
				 for details\n");
		return -1;
	}

	ps = ps_init(config);
	if (ps == NULL) {
		fprintf(stderr, "Failed to create recognizer, see log \
				 for details\n");
		return -1;
	}

	ps_free(ps);
	cmd_ln_free_r(config);

	return 0;
}
