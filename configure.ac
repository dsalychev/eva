#
# Eva
#
# Process this file with autoconf to produce a configure script.
#
# Copyright (C) 2016, Dmitry Salychev <darkness.bsd@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

AC_INIT([eva], [0.0.1], [darkness.bsd@gmail.com])
AM_INIT_AUTOMAKE([foreign subdir-objects])

# Check for sources directories
AC_CONFIG_SRCDIR([test/minunit.c])
AC_CONFIG_SRCDIR([lib/threadpool.c])
AC_CONFIG_SRCDIR([app/eva.c])

# Override default CFLAGS
CFLAGS="-Wall -pedantic -std=iso9899:1999 \
-Wshadow -Wpointer-arith -Wcast-qual -Wcast-align \
-Wstrict-prototypes -Wmissing-prototypes -Wconversion"

# Checks for programs.
AC_PROG_CC
AC_PROG_CC_C99

AC_USE_SYSTEM_EXTENSIONS

# Checks for libraries.
PKG_CHECK_MODULES([SPHINXBASE], [sphinxbase])
PKG_CHECK_MODULES([POCKETSPHINX], [pocketsphinx])

# Checks for header files.
AC_CHECK_HEADERS([fcntl.h stdint.h stdlib.h string.h sys/time.h unistd.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_CHECK_HEADER_STDBOOL
AC_TYPE_INT32_T
AC_TYPE_SIZE_T
AC_TYPE_UINT16_T
AC_TYPE_UINT32_T
AC_TYPE_UINT8_T

# Checks for library functions.
AC_CHECK_FUNCS([memset])

AC_CONFIG_FILES([Makefile])
AC_OUTPUT
