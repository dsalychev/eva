/*
 * Eva Utilities
 *
 * Copyright (C) 2016, Dmitry Salychev <darkness.bsd@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "args.h"
#include "assert.h"

argres_t getarg(int32_t argc, char *argv[], const char *optstring,
		arg_t *arg)
{
	if (!c_assert(argc > 1))
		return ARGRES_INVALID_INPUT;
	if (!c_assert(argv != NULL))
		return ARGRES_INVALID_INPUT;
	if (!c_assert(optstring != NULL))
		return ARGRES_INVALID_INPUT;
	if (!c_assert(arg != NULL))
		return ARGRES_INVALID_INPUT;

	int c;
	size_t optarg_l;

	c = getopt(argc, argv, optstring);
	if (c == -1) return ARGRES_NO_MORE_ARGS;

	switch (c) {
	case ':':
		printf("Option -%c requires an operand\n", optopt);
		return ARGRES_OPERAND_REQUIRED;
	case '?':
		printf("Unrecognized option: -%c\n", optopt);
		return ARGRES_UNREC_ARG;
	default:
		if (optarg != NULL) {
			optarg_l = strlen(optarg);
		} else {
			optarg_l = 0;
		}

		if (!c_assert(arg->optname_len > 0))
			return ARGRES_INVALID_INPUT;
		if (!c_assert(arg->optarg_len >= optarg_l + 1))
			return ARGRES_INVALID_INPUT;

		memset(arg->optname, 0, arg->optname_len);
		memset(arg->optarg, 0, arg->optarg_len);
		arg->optname[0] = (char) c;
		if (optarg_l > 0) {
			memcpy(arg->optarg, optarg, optarg_l + 1);
		}
	}
	return ARGRES_SUCCESS;
}
