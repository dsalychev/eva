/*
 * Eva Utilities
 *
 * Copyright (C) 2016, Dmitry Salychev <darkness.bsd@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _EVA_UTIL_ARGS_H_
#define _EVA_UTIL_ARGS_H_ 1

#include <stdlib.h>
#include <stdint.h>

typedef enum argres {
	ARGRES_SUCCESS = INT16_MIN,
	ARGRES_INVALID_INPUT,
	ARGRES_NO_MORE_ARGS,
	ARGRES_OPERAND_REQUIRED,
	ARGRES_UNREC_ARG,
} argres_t;

typedef struct argument {
	uint16_t optname_len;
	uint16_t optarg_len;
	char *optname;
	char *optarg;
} arg_t;

/* The function is a command-line parser which helps to get a
   specific argument and, if required, its value.

   This is an extended version of the getopt() function which returns
   status of the parsing operation.

   The parameters 'argc' and 'argv' are the argument count and argument
   array as passed to main(). The argument 'optstring' is a string of
   recognized option characters; if a character is followed by a colon,
   the option takes an argument. All option characters allowed
   by Utility Syntax Guideline 3 are allowed in 'optstring'.
   The implementation may accept other characters as an extension.

   The function doesn't dynamically allocate memory, thus it's required
   for the 'arg' parameter to be allocated. If the allocated
   length for 'arg.optname' or 'arg.optarg' isn't enough to store
   the parsed value, then E_INVALID_INPUT flag will be returned.

   If there are no command-line options to parse, E_NO_MORE_ARGS flag will be
   returned.

   If one of the given pointers is NULL, E_INVALID_INPUT flag will be
   returned.

   If 'argc' == 1 (no command-line arguments specified), then
   E_INVALID_INPUT flag will be returned.

   If option has been parsed without an argument (but it's required), then
   E_OPERAND_REQUIRED flag will be returned.

   If unknown option has been parsed, then E_UNREC_ARG flag will be returned.
   */
argres_t getarg(int32_t argc, char *argv[], const char *optstring,
		arg_t *arg);

#endif /* _EVA_UTIL_ARGS_H_ */
