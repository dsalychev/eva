/*
 * Eva Utilities
 *
 * Copyright (C) 2016, Dmitry Salychev <darkness.bsd@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>

#include "test/minunit.h"
#include "args.h"

#define SUITE_NAME		"args_test"

int tests_run = 0;

int should_parse_simple_arg(void);

int should_parse_simple_arg(void)
{
	const char *optstr = "c:h";
	char *argv[] = { "test_app\0", "-c\0", "val\0", "-h\0" };
	arg_t arg;
	argres_t res;
	char on[16];
	char oa[16];
	arg.optname = on;
	arg.optarg = oa;
	arg.optname_len = 16;
	arg.optarg_len = 16;

	while ((res = getarg(4, argv, optstr, &arg)) != ARGRES_NO_MORE_ARGS) {
		_mu_assert(res == ARGRES_SUCCESS);
		switch (arg.optname[0]) {
		case 'c':
			_mu_assert(strcmp("val", arg.optarg) == 0);
			break;
		case 'h':
			break;
		default:
			/* fail, unexpected option */
			_mu_assert(1 == 0);
		}
	}

	return 0;
}

int all_tests(void)
{
	_mu_verify(should_parse_simple_arg);
	return 0;
}

char *suite_name(void)
{
	return SUITE_NAME;
}
