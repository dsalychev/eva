/*
 * Eva Utilities
 *
 * Copyright (C) 2016, Dmitry Salychev <darkness.bsd@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _EVA_UTIL_ASSERT_H_
#define _EVA_UTIL_ASSERT_H_ 1

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#ifdef NDEBUG
/*
 * Assertion macros.
 *
 * It returns true value if the given condition is evaluated to true,
 * otherwise - macros returns false and assertion message will be printed.
 *
 * The macros is used if NDEBUG flag is set.
 */
#define c_assert(e)	((e)						\
			 ? (true)					\
			 : __ca_fail(__FILE__, __LINE__, #e))
#else
/*
 * Assertion macros.
 *
 * It returns true value if the given condition is evaluated to true,
 * otherwise - macros returns false and assertion message will be printed.
 *
 * This version is used for testing only.
 *
 * The macros is used if NDEBUG flag is not set.
 */
#define c_assert(e)	((e)						\
			 ? (true)					\
			 : __ca_fail(__FILE__, __LINE__, #e))
#endif /* NDEBUG */

bool __ca_fail(const char *file, uint32_t line, const char *ad);

#endif /* _EVA_UTIL_ASSERT_H_ */
