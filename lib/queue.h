/*
 * Eva Utilities
 *
 * Copyright (C) 2016, Dmitry Salychev <darkness.bsd@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _EVA_UTIL_QUEUE_H_
#define _EVA_UTIL_QUEUE_H_ 1

#include <stdint.h>
#include <stdbool.h>

/* Basic queue routines return codes */
typedef enum qresult {
	QRES_SUCCESS = INT16_MIN,
	QRES_INVALID_INPUT,
	QRES_ALLOC_FAIL,
	QRES_QUEUE_EMPTY,
	QRES_QUEUE_FULL,
} qresult_t;

/* Basic FIFO queue */
typedef struct queue {
	uint32_t capacity;		/* queue capacity */
	uint32_t head;			/* queue's first element */
	uint32_t tail;			/* queue's last element */
	uint32_t size;			/* actual number of elements in
					   the queue */
	void **base_arr;		/* queue base array */
} queue_t;

qresult_t q_init(queue_t *q, void **q_arr, uint32_t q_arr_size);
qresult_t q_enq(queue_t *q, void *elem);
qresult_t q_deq(queue_t *q, void **elem);
bool q_has_next(const queue_t *q);
bool q_is_empty(const queue_t *q);

#endif /* _EVA_UTIL_QUEUE_H_ */
