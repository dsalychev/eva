/*
 * Eva Utilities
 *
 * Copyright (C) 2016, Dmitry Salychev <darkness.bsd@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "queue.h"
#include "test/minunit.h"

#define SUITE_NAME		"queue_test"
#define QUEUE_SIZE		8

int tests_run = 0;

int queue_should_enqueue_dequeue_elements(void);

int queue_should_enqueue_dequeue_elements(void)
{
	void *q_arr[QUEUE_SIZE];
	queue_t q;
	void *elem = NULL;
	qresult_t r;

	r = q_init(&q, q_arr, QUEUE_SIZE);
	_mu_assert(r == QRES_SUCCESS);
	_mu_assert(q.size == 0);
	_mu_assert(q.head == 0);
	_mu_assert(q.tail == 0);
	_mu_assert(q.capacity == QUEUE_SIZE);

	r = q_enq(&q, "element");
	_mu_assert(r == QRES_SUCCESS);
	_mu_assert(q.size == 1);
	_mu_assert(q.head == 0);
	_mu_assert(q.tail == 1);
	_mu_assert(q.base_arr[q.head] != 0);

	r = q_deq(&q, &elem);
	_mu_assert(r == QRES_SUCCESS);
	_mu_assert(q.size == 0);
	_mu_assert(q.head == 1);
	_mu_assert(q.tail == 1);
	_mu_assert(elem != NULL);

	return 0;
}

int all_tests(void)
{
	_mu_verify(queue_should_enqueue_dequeue_elements);
	return 0;
}

char *suite_name(void)
{
	return SUITE_NAME;
}
