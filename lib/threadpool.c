/*
 * Eva Thread Pool
 *
 * This pool guarantees that it doesn't dynamically allocate memory and
 * doesn't use recursion in order to satisfy MISRA C rules.
 *
 * Copyright (C) 2016, Dmitry Salychev <darkness.bsd@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/time.h>
#include <fcntl.h>

#include "threadpool.h"
#include "assert.h"
#include "queue.h"

#define POSITIVE		75
#define LOW32			0x00000000FFFFFFFF
#define TASKS_SEM		"signal_tasks_sem"

/* Private prototypes */
static void *process_task(void *arg);

/* Thread function processes tasks from the queue */
static void *process_task(void *arg)
{
	if (!c_assert(arg != NULL)) {
		printf("Thread function called with NULL argument");
		pthread_exit(NULL);
	}

	struct threadpool *pool;
	struct threadpool_queue *pq;
	struct threadpool_task *task;
	void *(*task_func)(void *arg);
	void *task_func_arg;
	uint8_t keep_alive;
	enum qresult rq;

	pool = (struct threadpool *) arg;
	pq = pool->q;
	pthread_mutex_lock(&pq->rw_mutex);
	keep_alive = pool->keep_alive;
	pthread_mutex_unlock(&pq->rw_mutex);

	while (keep_alive) {
		/* Wait for a task to be available */
		sem_wait(pool->tasks_sem);

		/* Retrieve the task */
		pthread_mutex_lock(&pq->rw_mutex);
		keep_alive = pool->keep_alive;
		rq = q_deq(pq->q, (void **) &task);
		pthread_mutex_unlock(&pq->rw_mutex);

		if (!keep_alive)
			pthread_exit(NULL);
		if (rq != QRES_SUCCESS)
			continue;
		task_func = task->func;
		task_func_arg = task->arg;
		task_func(task_func_arg);
	}
	pthread_exit(NULL);
}

enum threadpool_res threadpool_init(
		struct threadpool *tp,
		struct threadpool_queue *q,
		uint32_t nthreads,
		pthread_t *threads)
{
	if (!c_assert(tp != NULL))
		return THREADPOOL_INVALID_INPUT;
	if (!c_assert(q != NULL))
		return THREADPOOL_INVALID_INPUT;
	if (!c_assert(nthreads > 0))
		return THREADPOOL_INVALID_INPUT;
	if (!c_assert(threads != NULL))
		return THREADPOOL_INVALID_INPUT;

	int rc;
	uint32_t i;
	pthread_attr_t attr;

	/*
	 * Initialize semaphore to signal threads that there is a
	 * task to process.
	 */
	tp->tasks_sem = sem_open(TASKS_SEM, O_CREAT, 0600, 0);
	if (tp->tasks_sem == SEM_FAILED) {
		/*
		 * Do not leave thread pool partially initialized!
		 */
		return THREADPOOL_SEM_CREATE_ERR;
	}

	/* Initialize and set thread detached attribute */
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	tp->q = q;
	tp->nthreads = nthreads;
	tp->threads = threads;
	pthread_mutex_lock(&q->rw_mutex);
	tp->keep_alive = POSITIVE;
	pthread_mutex_unlock(&q->rw_mutex);
	for (i = 0; i < nthreads; i++) {
		rc = pthread_create(&threads[i], &attr, process_task, tp);
		if (rc) {
			/*
			 * Do not leave thread pool partially initialized!
			 */
			return THREADPOOL_THREAD_CREATE_ERR;
		}
	}

	pthread_attr_destroy(&attr);
	return THREADPOOL_SUCCESS;
}

enum threadpool_res threadpool_destroy(struct threadpool *tp)
{
	if (!c_assert(tp != NULL))
		return THREADPOOL_INVALID_INPUT;

	int rc;
	uint32_t i;
	void *status;

	/* Tell all threads to finish their work */
	pthread_mutex_lock(&tp->q->rw_mutex);
	tp->keep_alive = 0;
	pthread_mutex_unlock(&tp->q->rw_mutex);
	for (i = 0; i < tp->nthreads; i++) {
		sem_post(tp->tasks_sem);
	}

	/* Wait for all threads, one by one */
	for (i = 0; i < tp->nthreads; i++) {
		rc = pthread_join(tp->threads[i], &status);
		if (rc)
			printf("Thread hasn't been finished successfully");
	}
	/* Destroy threads semaphore */
	rc = sem_close(tp->tasks_sem);
	if (rc)
		printf("Threads semaphore hasn't been closed successfully");

	tp->q = NULL;
	tp->nthreads = 0;
	tp->threads = NULL;
	return THREADPOOL_SUCCESS;
}

enum threadpool_res threadpool_queue_init(
		struct threadpool_queue *pq,
		struct queue *q)
{
	if (!c_assert(pq != NULL))
		return THREADPOOL_INVALID_INPUT;
	if (!c_assert(q != NULL))
		return THREADPOOL_INVALID_INPUT;

	pq->q = q;
	pthread_mutex_init(&pq->rw_mutex, NULL);
	return THREADPOOL_SUCCESS;
}

enum threadpool_res threadpool_queue_destroy(struct threadpool_queue *pq)
{
	if (!c_assert(pq != NULL))
		return THREADPOOL_INVALID_INPUT;

	pq->q = NULL;
	pthread_mutex_destroy(&pq->rw_mutex);
	return THREADPOOL_SUCCESS;
}

enum threadpool_res threadpool_task_init(
		struct threadpool_task *task,
		void *(*func)(void *arg),
		void *arg)
{
	if (!c_assert(task != NULL))
		return THREADPOOL_INVALID_INPUT;
	if (!c_assert(func != NULL))
		return THREADPOOL_INVALID_INPUT;

	task->func = func;
	task->arg = arg;
	return THREADPOOL_SUCCESS;
}

enum threadpool_res threadpool_submit(
		struct threadpool *tp,
		struct threadpool_task *task)
{
	if (!c_assert(tp != NULL))
		return THREADPOOL_INVALID_INPUT;
	if (!c_assert(task != NULL))
		return THREADPOOL_INVALID_INPUT;

	enum qresult res;

	pthread_mutex_lock(&tp->q->rw_mutex);
	res = q_enq(tp->q->q, task);
	pthread_mutex_unlock(&tp->q->rw_mutex);
	sem_post(tp->tasks_sem);

	if (res != QRES_SUCCESS)
		return THREADPOOL_INVALID_INPUT;
	return THREADPOOL_SUCCESS;
}
