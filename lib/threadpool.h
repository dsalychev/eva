/*
 * Eva Thread Pool
 *
 * This pool guarantees that it doesn't dynamically allocate memory and
 * doesn't use recursion in order to satisfy MISRA C rules.
 *
 * Copyright (C) 2016, Dmitry Salychev <darkness.bsd@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _EVA_THREADPOOL_H_
#define _EVA_THREADPOOL_H_ 1

#include <stdint.h>
#include <pthread.h>
#include <semaphore.h>

#include "queue.h"

/* Result of the thread pool operations */
enum threadpool_res {
	THREADPOOL_SUCCESS = INT16_MIN,
	THREADPOOL_INVALID_INPUT,
	THREADPOOL_THREAD_CREATE_ERR,
	THREADPOOL_SEM_CREATE_ERR,
};

struct threadpool_task {
	void *(*func)(void *arg);	/* pointer to the task function */
	void *arg;			/* argument for the task function */
};

struct threadpool_queue {
	pthread_mutex_t rw_mutex;	/* queue r/w access lock */
	queue_t *q;			/* shared queue of tasks */
};

struct threadpool {
	uint32_t nthreads;		/* number of allocated threads */
	uint8_t volatile keep_alive;	/* set to positive - to keep threads
					   alive; queue of the pool should be
					   locked to access the flag */
	sem_t *tasks_sem;		/* semaphore to unlock suspended worker
					   threads */
	pthread_t *threads;		/* array of the threads */
	struct threadpool_queue *q;	/* tasks queue of the thread pool */
};

enum threadpool_res threadpool_init(
		struct threadpool *tp,
		struct threadpool_queue *q,
		uint32_t nthreads,
		pthread_t *threads);
enum threadpool_res threadpool_destroy(struct threadpool *tp);

enum threadpool_res threadpool_queue_init(
		struct threadpool_queue *pq,
		struct queue *q);
enum threadpool_res threadpool_queue_destroy(struct threadpool_queue *pq);

enum threadpool_res threadpool_task_init(
		struct threadpool_task *task,
		void *(*func)(void *arg),
		void *arg);

enum threadpool_res threadpool_wait(struct threadpool *pool);

enum threadpool_res threadpool_submit(
		struct threadpool *tp,
		struct threadpool_task *task);

#endif /* _EVA_THREADPOOL_H_ */
