/*
 * Eva Thread Pool
 *
 * This pool guarantees that it doesn't dynamically allocate memory and
 * recursion in order to satisfy MISRA C rules.
 *
 * Copyright (C) 2016, Dmitry Salychev <darkness.bsd@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <pthread.h>

#include "test/minunit.h"
#include "threadpool.h"
#include "queue.h"

#define SUITE_NAME		"threadpool_test"
#define THREADPOOL_TASKS	32
#define THREADPOOL_THREADS	16

#define TASKS_TO_PROCESS	32

/* Tests counter */
int tests_run = 0;

/* Test functions prototypes */
int threadpool_should_be_initialized(void);
int task_should_be_submitted(void);

/* Private prototypes */
static void *test_thread_func(void *arg);

/* Private variables */
static uint16_t task_processed = TASKS_TO_PROCESS;
static pthread_mutex_t task_processed_mutex;
static pthread_cond_t task_processed_cv;

int threadpool_should_be_initialized(void)
{
	const uint32_t attempts = 12;
	uint32_t i;

	for (i = 0; i < attempts; i++) {
		pthread_t threads[THREADPOOL_THREADS];
		void *q_array[THREADPOOL_TASKS];
		struct queue q;
		struct threadpool pool;
		struct threadpool_queue pool_queue;
		enum threadpool_res res;

		q_init(&q, q_array, THREADPOOL_TASKS);
		res = threadpool_queue_init(&pool_queue, &q);
		_mu_test(res == THREADPOOL_SUCCESS);
		_mu_test(pool_queue.q == &q);

		res = threadpool_init(&pool, &pool_queue, THREADPOOL_THREADS,
				      threads);
		_mu_test(res == THREADPOOL_SUCCESS);
		_mu_test(pool.q == &pool_queue);
		_mu_test(pool.nthreads == THREADPOOL_THREADS);
		_mu_test(pool.threads == threads);

		threadpool_destroy(&pool);
		threadpool_queue_destroy(&pool_queue);
	}
	return 0;
}

int task_should_be_submitted(void)
{
	pthread_t threads[THREADPOOL_THREADS];
	void *q_array[THREADPOOL_TASKS];
	struct queue q;
	struct threadpool pool;
	struct threadpool_queue pool_queue;
	struct threadpool_task pool_tasks[TASKS_TO_PROCESS];
	int i;

	/* Initialize mutex and conditional variable */
	pthread_mutex_init(&task_processed_mutex, NULL);
	pthread_cond_init(&task_processed_cv, NULL);

	/* Initialize queue and thread pool */
	q_init(&q, q_array, THREADPOOL_TASKS);
	threadpool_queue_init(&pool_queue, &q);
	threadpool_init(&pool, &pool_queue, THREADPOOL_THREADS, threads);

	for (i = 0; i < TASKS_TO_PROCESS; i++) {
		threadpool_task_init(&pool_tasks[i], test_thread_func, NULL);
		threadpool_submit(&pool, &pool_tasks[i]);
	}

	/* Wait for the tasks to be processed */
	pthread_mutex_lock(&task_processed_mutex);
	while (task_processed > 0) {
		pthread_cond_wait(&task_processed_cv, &task_processed_mutex);
	}
	pthread_mutex_unlock(&task_processed_mutex);

	/* Clean up and finish the test */
	threadpool_destroy(&pool);
	threadpool_queue_destroy(&pool_queue);
	pthread_cond_destroy(&task_processed_cv);
	pthread_mutex_destroy(&task_processed_mutex);
	return 0;
}

int all_tests(void)
{
	_mu_verify(threadpool_should_be_initialized);
	_mu_verify(task_should_be_submitted);
	return 0;
}

char *suite_name(void)
{
	return SUITE_NAME;
}

static void *test_thread_func(void *arg)
{
	pthread_mutex_lock(&task_processed_mutex);
	printf("  test_thread_func: task %d finished\n", task_processed);
	task_processed--;
	pthread_cond_broadcast(&task_processed_cv);
	pthread_mutex_unlock(&task_processed_mutex);

	return 0;
}
