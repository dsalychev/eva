/*
 * Eva Text Routines
 *
 * Copyright (C) 2016, Dmitry Salychev <darkness.bsd@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ftw.h>

#include "text/textcorpus.h"
#include "lib/args.h"

#define OPTSTR		":f:h"
#define UNKNOWN_OPT	"Unknown option, -h for usage\r\n"

int file_process(const char *path, const struct stat *stat,
		 int flags, struct FTW *ftw);

int main(int argc, char *argv[])
{
	arg_t arg;
	argres_t r;
	int fd_limit = 5;
	int flags = FTW_CHDIR | FTW_DEPTH | FTW_MOUNT;

	char on[16];
	char oa[16];
	arg.optname = on;
	arg.optname_len = 16;
	arg.optarg = oa;
	arg.optarg_len = 16;

	while ((r = getarg(argc, argv, OPTSTR, &arg)) != ARGRES_NO_MORE_ARGS) {
		if (r == ARGRES_SUCCESS) {
			printf(" Arg: name=%s, value=%s\n",
			       arg.optname, arg.optarg);
		}
	}

	return nftw(arg.optarg, file_process, fd_limit, flags);
}

int file_process(const char *path, const struct stat *stat,
		 int flags, struct FTW *ftw)
{
	printf(" %s\r\n", path);
	return 0;
}

